﻿/*
    Copyright © 2010 The Divinity Project; 2013-2016 Dignity Team.
	All rights reserved.
	http://board.thedivinityproject.com
	http://www.ragezone.com


	This file is part of Ostara.

	Ostara is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Ostara is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Ostara.  If not, see <http://www.gnu.org/licenses/>.
*/

#region Includes

using System.Collections.Generic;

#endregion

namespace CabalPacketPal
{
	class Configuration
	{
		public static List<ushort> ChatPorts = new List<ushort>();
		public static List<ushort> LoginPorts = new List<ushort>();
		public static List<ushort> WorldPortsStart = new List<ushort>();
		public static List<ushort> WorldPortsEnd = new List<ushort>();

		public static void Load()
		{
			var ini = new IniReader("Config.ini");

			string[] chat = ini.ReadString("Ports", "Chat").Split(',');
			string[] login = ini.ReadString("Ports", "Login").Split(',');
			string[] worldStart = ini.ReadString("Ports", "WorldStart").Split(',');
			string[] worldEnd = ini.ReadString("Ports", "WorldEnd").Split(',');

			foreach (var s in chat)
				ChatPorts.Add(ushort.Parse(s));
			foreach (var s in login)
				LoginPorts.Add(ushort.Parse(s));
			foreach (var s in worldStart)
				WorldPortsStart.Add(ushort.Parse(s));
			foreach (var s in worldEnd)
				WorldPortsEnd.Add(ushort.Parse(s));
		}
	}
}