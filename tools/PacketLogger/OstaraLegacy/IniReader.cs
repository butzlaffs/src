﻿/*
    Copyright © 2010 The Divinity Project; 2013-2016 Dignity Team.
	All rights reserved.
	http://board.thedivinityproject.com
	http://www.ragezone.com


	This file is part of Ostara.

	Ostara is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Ostara is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Ostara.  If not, see <http://www.gnu.org/licenses/>.
*/

#region Includes

using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace CabalPacketPal
{
	class IniReader
	{
		[DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileIntA", CharSet = CharSet.Ansi)]
		private static extern int GetPrivateProfileInt(string lpApplicationName, string lpKeyName, int nDefault, string lpFileName);
		[DllImport("KERNEL32.DLL", EntryPoint = "WritePrivateProfileStringA", CharSet = CharSet.Ansi)]
		private static extern int WritePrivateProfileString(string lpApplicationName, string lpKeyName, string lpString, string lpFileName);
		[DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileStringA", CharSet = CharSet.Ansi)]
		private static extern int GetPrivateProfileString(string lpApplicationName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);
		[DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileSectionNamesA", CharSet = CharSet.Ansi)]
		private static extern int GetPrivateProfileSectionNames(byte[] lpszReturnBuffer, int nSize, string lpFileName);
		[DllImport("KERNEL32.DLL", EntryPoint = "WritePrivateProfileSectionA", CharSet = CharSet.Ansi)]
		private static extern int WritePrivateProfileSection(string lpAppName, string lpString, string lpFileName);

		private string _filename;
		private string _section;
		private const int MAX_ENTRY = 32768;

		public IniReader(string file)
		{ _filename = Directory.GetCurrentDirectory() + @"\" + file; }

		public string Filename
		{
			get { return _filename; }
			set { _filename = value; }
		}

		public string Section
		{
			get { return _section; }
			set { _section = value; }
		}

		public int ReadInt32(string section, string key, int defVal)
			{ return GetPrivateProfileInt(section, key, defVal, _filename); }

		public int ReadInt32(string section, string key)
			{ return ReadInt32(section, key, 0); }

		public int ReadInt32(string key, int defVal)
			{ return ReadInt32(_section, key, defVal); }

		public int ReadInt32(string key)
			{ return ReadInt32(key, 0); }

		public string ReadString(string section, string key, string defVal)
		{
			StringBuilder sb = new StringBuilder(MAX_ENTRY);
			int Ret = GetPrivateProfileString(section, key, defVal, sb, MAX_ENTRY, _filename);
			return sb.ToString();
		}

		public string ReadString(string section, string key)
			{ return ReadString(section, key, ""); }

		public string ReadString(string key)
			{ return ReadString(_section, key); }

		public long ReadInt64(string section, string key, long defVal)
			{ return long.Parse(ReadString(section, key, defVal.ToString())); }

		public long ReadInt64(string section, string key)
			{ return ReadInt64(section, key, 0); }

		public long ReadInt64(string key, long defVal)
			{ return ReadInt64(_section, key, defVal); }

		public long ReadInt64(string key)
			{ return ReadInt64(key, 0); }

		public byte[] ReadByteArray(string section, string key)
		{
			try { return Convert.FromBase64String(ReadString(section, key)); }
			catch { }

			return null;
		}

		public byte[] ReadByteArray(string key)
			{ return ReadByteArray(_section, key); }

		public bool ReadBoolean(string section, string key, bool defVal)
			{ return Boolean.Parse(ReadString(section, key, defVal.ToString())); }

		public bool ReadBoolean(string section, string key)
			{ return ReadBoolean(section, key, false); }

		public bool ReadBoolean(string key, bool defVal)
			{ return ReadBoolean(_section, key, defVal); }

		public bool ReadBoolean(string key)
			{ return ReadBoolean(Section, key); }

		public ArrayList GetSectionNames()
		{
			try
			{
				byte[] buffer = new byte[MAX_ENTRY];
				GetPrivateProfileSectionNames(buffer, MAX_ENTRY, Filename);
				string[] parts = Encoding.ASCII.GetString(buffer).Trim('\0').Split('\0');

				return new ArrayList(parts);
			}
			catch { }

			return null;
		}
	}
}