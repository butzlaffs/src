﻿/*
    Copyright © 2010 The Divinity Project; 2013-2016 Dignity Team.
	All rights reserved.
	http://board.thedivinityproject.com
	http://www.ragezone.com


	This file is part of Ostara.

	Ostara is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Ostara is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Ostara.  If not, see <http://www.gnu.org/licenses/>.
*/

#region Includes

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using PacketDotNet;
using SharpPcap;
using SharpPcap.LibPcap;

#endregion

namespace CabalPacketPal
{
	public partial class MainForm : Form
	{
		PcapDevice device;
		List<byte[]> packets;

		Dictionary<ushort, Cryption.Client> clientCryption;
		Dictionary<ushort, Cryption.Server> serverCryption;
		Dictionary<ushort, ushort> clientPorts;

		string filter;

		byte[] partiali;
		byte[] partialo;

		List<ushort> ignore;

		bool paused;

		public MainForm()
		{
			InitializeComponent();

			ignore = new List<ushort>();

			if (File.Exists("Ignore.op"))
			{
				var file = File.OpenRead("Ignore.op");
				var r = new BinaryReader(file, System.Text.Encoding.ASCII);

				while (r.PeekChar() != -1)
					ignore.Add(r.ReadUInt16());

				r.Dispose();
			}

			Configuration.Load();

			for (int i = 0; i < Configuration.LoginPorts.Count; i++)
				filter += String.Format("tcp port {0} or ", Configuration.LoginPorts[i]);
			for (int i = 0; i < Configuration.WorldPortsStart.Count; i++)
				for (int j = Configuration.WorldPortsStart[i]; j <= Configuration.WorldPortsEnd[i]; j++)
					filter += String.Format("tcp port {0} or ", j);
			for (int i = 0; i < Configuration.ChatPorts.Count; i++)
				filter += (i == Configuration.ChatPorts.Count - 1) ?
						  String.Format("tcp port {0}", Configuration.ChatPorts[i]) :
						  String.Format("tcp port {0} or ", Configuration.ChatPorts[i]);

			foreach (var d in LibPcapLiveDeviceList.Instance)
				comboDevices.Items.Add(d.Description);

			comboDevices.SelectedIndex = 0;

            comboBox1.SelectedIndex = 0;
		}

		void device_OnPacketArrival(object sender, CaptureEventArgs e)
		{
			if (this.dgvPackets.InvokeRequired)
			{
				var d = new ListPacketCallback(ListPacket);
				this.Invoke(d, new object[] { e.Packet });
			}
			else
				ListPacket(e.Packet);
		}

		void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (device != null && device.Opened)
			{
				device.StopCapture();
				device.Close();
			}

			var file = File.OpenWrite("Ignore.op");
			var w = new BinaryWriter(file);

			foreach (var i in ignore)
				w.Write(i);

			w.Flush();
			w.Dispose();
		}

		void dgvPackets_RowEnter(object sender, DataGridViewCellEventArgs e)
		{
			if (this.rtxtHex.InvokeRequired)
			{
				var d = new ShowPacketCallback(ShowPacket);
				this.Invoke(d, new object[] { e.RowIndex });
			}
			else
				ShowPacket(e.RowIndex);
		}

		void btnGo_Click(object sender, EventArgs e)
		{
			if (device == null || !device.Opened)
			{
				clientCryption = new Dictionary<ushort, Cryption.Client>();
				serverCryption = new Dictionary<ushort, Cryption.Server>();
				clientPorts = new Dictionary<ushort, ushort>();

				dgvPackets.Rows.Clear();

				int index = comboDevices.SelectedIndex;

				device = LibPcapLiveDeviceList.Instance[index];
				packets = new List<byte[]>();

				device.OnPacketArrival += device_OnPacketArrival;

				device.Open();

				device.Filter = filter;
				device.StartCapture();

				btnGo.Text = "Stop Logging";
				btnGo.BackColor = Color.Firebrick;
				lblNI.Enabled = false;
				comboDevices.Enabled = false;
			}
			else
			{
				device.OnPacketArrival -= device_OnPacketArrival;

				device.StopCapture();
				device.Close();

				btnGo.Text = "Start Logging";
				btnGo.BackColor = Color.ForestGreen;
				lblNI.Enabled = true;
				comboDevices.Enabled = true;
			}
		}

		void ListPacket(RawCapture packet)
		{
			var subpackets = new List<byte[]>();

			var enet = (EthernetPacket)Packet.ParsePacket(LinkLayers.Ethernet, packet.Data);
			var ip = (IpPacket)(enet).PayloadPacket;
			var tcp = (TcpPacket)(ip).PayloadPacket;
			var data = tcp.PayloadData;

			if (data.Length != 0)
			{
				bool isIncoming = IsIncoming(ref tcp);
				bool isChat = IsChat(ref tcp);
				bool isLogin = IsLogin(ref tcp);
				bool isWorld = IsWorld(ref tcp);

				ushort port = (isIncoming) ? tcp.SourcePort : tcp.DestinationPort;

				if (!clientCryption.ContainsKey(port))
				{
					clientCryption.Add(port, new Cryption.Client());
					serverCryption.Add(port, new Cryption.Server());

					clientPorts.Add(port, tcp.SourcePort);
				}

				if ((isIncoming && clientPorts[port] != tcp.DestinationPort) ||
					(!isIncoming && clientPorts[port] != tcp.SourcePort))
					return;

				if (!isIncoming)
				{
					while (true)
					{
						int size = (data.Length >= 4) ? clientCryption[port].GetPacketSize(ref data) : 4;
						var p2 = new byte[size];

						if (size > data.Length)
						{
							if (partialo == null)
							{
								partialo = data;
								break;
							}
							else
							{
								var i = partialo.Length;
								Array.Resize(ref partialo, data.Length + partialo.Length);
								Array.ConstrainedCopy(data, 0, partialo, i, data.Length);
								data = partialo;
								partialo = null;
								size = clientCryption[port].GetPacketSize(ref data);

								if (size > data.Length)
								{
									partialo = data;
									break;
								}
							}
						}

						if (size != data.Length)
							Array.ConstrainedCopy(data, 0, p2, 0, size);
						else
							p2 = data;

						clientCryption[port].Decrypt(ref p2);

						subpackets.Add(p2);

						if (size == data.Length)
							break;
						else
						{
							var left = new byte[data.Length - size];
							Array.ConstrainedCopy(data, size, left, 0, data.Length - size);
							data = left;
						}
					}
				}
				else
				{
					while (true)
					{
						int size = (data.Length >= 4) ? serverCryption[port].GetPacketSize(ref data) : 4;
						var p2 = new byte[size];

						if (size > data.Length)
						{
							if (partiali == null)
							{
								partiali = data;
								break;
							}
							else
							{
								var i = partiali.Length;
								Array.Resize(ref partiali, data.Length + partiali.Length);
								Array.ConstrainedCopy(data, 0, partiali, i, data.Length);
								data = partiali;
								partiali = null;
								size = serverCryption[port].GetPacketSize(ref data);

								if (size > data.Length)
								{
									partiali = data;
									break;
								}
							}
						}

						if (size != data.Length)
							Array.ConstrainedCopy(data, 0, p2, 0, size);
						else
							p2 = data;

						serverCryption[port].Decrypt(ref p2);

						var opcode = BitConverter.ToUInt16(p2, 0x04);

						if ((opcode == 0x191 && isChat) ||
							(opcode == 0x65 && isLogin) ||
							(opcode == 0x8C && isWorld))		// Connect2Svr
						{
							var key = BitConverter.ToUInt32(p2, 0x6);
							var step = BitConverter.ToUInt16(p2, 0x10);

							serverCryption[port].ChangeKey(key, step);
							clientCryption[port].ChangeKey(key, step);
						}

						subpackets.Add(p2);

						if (size == data.Length)
							break;
						else
						{
							var left = new byte[data.Length - size];
							Array.ConstrainedCopy(data, size, left, 0, data.Length - size);
							data = left;
						}
					}
				}

				if (paused)
					return;

				foreach (var p in subpackets)
				{
					if (BitConverter.ToUInt16(p, 0) != 0xB7E2)
						continue;

					var opcode = (isIncoming) ? BitConverter.ToUInt16(p, 0x04) : BitConverter.ToUInt16(p, 0x08);

					if (ignore.Contains(opcode))
						continue;

					packets.Add(p);

					string op = "0x";
					op += (isIncoming) ?
						  BitConverter.ToUInt16(p, 0x04).ToString("X2") :
						  BitConverter.ToUInt16(p, 0x08).ToString("X2");
					if (isChat)
						op += String.Format(" - {0}", (isIncoming) ?
													  (Opcodes.Chat)BitConverter.ToUInt16(p, 0x04) :
													  (Opcodes.Chat)BitConverter.ToUInt16(p, 0x08));
					else if (isLogin)
						op += String.Format(" - {0}", (isIncoming) ?
													  (Opcodes.Login)BitConverter.ToUInt16(p, 0x04) :
													  (Opcodes.Login)BitConverter.ToUInt16(p, 0x08));
					else if (isWorld)
						op += String.Format(" - {0}", (isIncoming) ?
													  (Opcodes.World)BitConverter.ToUInt16(p, 0x04) :
													  (Opcodes.World)BitConverter.ToUInt16(p, 0x08));
					else
						op += " - Unknown Source/Destination";

					var row = new DataGridViewRow();
					row.CreateCells(dgvPackets,
									packet.Timeval.Date,
									p.Length,
									ip.SourceAddress,
									tcp.SourcePort,
									ip.DestinationAddress,
									tcp.DestinationPort,
									op);

					row.HeaderCell.ValueType = typeof(String);
					row.HeaderCell.Value = (isChat) ?
										   "Chat" :
										   (isLogin) ?
										   "Login" :
										   "World";

					row.DefaultCellStyle.BackColor = Color.Black;
					row.DefaultCellStyle.ForeColor = (isIncoming) ?
													 Color.LightGreen :
													 Color.LightBlue;

					row.DefaultCellStyle.SelectionBackColor = (isIncoming) ?
															  Color.LightGreen :
															  Color.LightBlue;
					row.DefaultCellStyle.SelectionForeColor = Color.Black;

                    row.Visible = false;

                    if (comboBox1.SelectedIndex == 1 && isLogin)
                        row.Visible = true;
                    else if (comboBox1.SelectedIndex == 2 && isWorld)
                        row.Visible = true;
                    else if (comboBox1.SelectedIndex == 3 && isChat)
                        row.Visible = true;
                    else if (comboBox1.SelectedIndex == 0)
                        row.Visible = true;

					dgvPackets.Rows.Add(row);

				}
			}
		}
		delegate void ListPacketCallback(RawCapture packet);

		void ShowPacket(int index)
		{
			string lines = "00000000";
			string hex = "";
			string text = "";
			int rows = 1;

			for (int i = 0; i < packets[index].Length; i++)
			{
				hex += packets[index][i].ToString("X2") + " ";
				text += ((char)packets[index][i] != 0) ? (char)packets[index][i] : '.';

				double newline = (double)(i + 1) / 16d;
				newline -= Math.Floor(newline);

				if (newline == 0)
				{
					hex += "\r\n";
					text += "\r\n";
					rows += 1;
				}
			}

			for (int i = 1; i < rows; i++)
				lines += "\r\n" + (i * 16).ToString("X8");

			txtLines.Text = lines;
			rtxtHex.Text = hex;
			txtText.Text = text;
		}
		delegate void ShowPacketCallback(int index);

		bool IsIncoming(ref TcpPacket packet)
		{
			bool result = false;

			for (int i = 0; i < Configuration.WorldPortsStart.Count; i++)
			{
				if (packet.SourcePort >= Configuration.WorldPortsStart[i] &&
					packet.SourcePort <= Configuration.WorldPortsEnd[i])
				{
					result = true;
					break;
				}
			}

			return (Configuration.ChatPorts.Contains(packet.SourcePort) ||
					Configuration.LoginPorts.Contains(packet.SourcePort) ||
					result);
		}

		bool IsOutgoing(ref TcpPacket packet)
		{
			bool result = false;

			for (int i = 0; i < Configuration.WorldPortsStart.Count; i++)
			{
				if (packet.DestinationPort >= Configuration.WorldPortsStart[i] &&
					packet.DestinationPort <= Configuration.WorldPortsEnd[i])
				{
					result = true;
					break;
				}
			}

			return (Configuration.ChatPorts.Contains(packet.DestinationPort) ||
					Configuration.LoginPorts.Contains(packet.DestinationPort) ||
					result);
		}

		bool IsChat(ref TcpPacket packet)
		{
			return (Configuration.ChatPorts.Contains(packet.SourcePort) ||
					Configuration.ChatPorts.Contains(packet.DestinationPort));
		}

		bool IsLogin(ref TcpPacket packet)
		{
			return (Configuration.LoginPorts.Contains(packet.SourcePort) ||
					Configuration.LoginPorts.Contains(packet.DestinationPort));
		}

		bool IsWorld(ref TcpPacket packet)
		{
			for (int i = 0; i < Configuration.WorldPortsStart.Count; i++)
			{
				if ((packet.SourcePort >= Configuration.WorldPortsStart[i] && packet.SourcePort <= Configuration.WorldPortsEnd[i]) ||
					(packet.DestinationPort >= Configuration.WorldPortsStart[i] && packet.DestinationPort <= Configuration.WorldPortsEnd[i]))
					return true;
			}

			return false;
		}

		void dgvPackets_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Right)
			{
				if (e.RowIndex == -1 || e.ColumnIndex == -1)
					return;

				dgvPackets.ClearSelection();
				dgvPackets.Rows[e.RowIndex].Selected = true;
				cmsRow.Show(MousePosition);
			}
		}

		void tsmiAddIgnore_Click(object sender, EventArgs e)
		{
			var inc = (packets[dgvPackets.SelectedRows[0].Index][4] != 0);
			var opcode = BitConverter.ToUInt16(packets[dgvPackets.SelectedRows[0].Index], (inc) ? 4 : 8);
			ignore.Add(opcode);

			var remove = new List<int>();

			for (int i = 0; i < packets.Count; i++)
			{
				inc = (packets[i][4] != 0);
				var op = BitConverter.ToUInt16(packets[i], (inc) ? 4 : 8);

				if (op == opcode)
					remove.Add(i);
			}

			for (int i = remove.Count - 1; i >= 0; i--)
			{
				packets.RemoveAt(remove[i]);
				dgvPackets.Rows.RemoveAt(remove[i]);
			}
		}

		void tsmiClearIgnore_Click(object sender, EventArgs e)
		{
			ignore.Clear();
		}

		void tsmiEditSave_Click(object sender, EventArgs e)
		{
			var n = 0;
			var f = "";

			while (true)
			{
				f = String.Format("Packets{0}.ost", n);

				if (!File.Exists(f))
					break;

				n++;
			}

			var file = File.OpenWrite(f);
			var w = new BinaryWriter(file);

			w.Write(dgvPackets.Rows.Count);	// int packet_count

			for (int i = 0; i < dgvPackets.Rows.Count; i++)	//	Packet packets[packet_count]
			{
				var row = dgvPackets.Rows[i];
				var header = row.HeaderCell;
				var opcode = (string)row.Cells["Opcode"].Value;

				w.Write(((string)header.Value == "Login") ? (byte)0 : ((string)header.Value == "World") ? (byte)1 : (byte)2);	// Daemon daemon
				w.Write((uint)(DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds);	// time_t time
				w.Write((packets[i][4] == 0) ? (byte)0 : (byte)1);	// Direction direction
				w.Write((byte)opcode.Length);	// byte opcode_length
				w.Write(System.Text.Encoding.ASCII.GetBytes(opcode));	// char opcode[opcode_length]
				w.Write((ushort)packets[i].Length);	// ushort data_length
				w.Write(packets[i]); // byte data[data_length]
			}

			w.Flush();
			w.Dispose();

			MessageBox.Show("Saved log as " + f, "Success!");
		}

		void tsmiEditClear_Click(object sender, EventArgs e)
		{
			dgvPackets.Rows.Clear();
			packets.Clear();

			if (paused)
				PauseUnpause();
		}

		void tsmiEditPause_Click(object sender, EventArgs e)
		{
			PauseUnpause();
		}

		void PauseUnpause()
		{
			paused = !paused;

			if (paused)
				tsmiEditPause.Text = "Resume Logging";
			else
				tsmiEditPause.Text = "Pause Logging";
		}

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = "";

            switch (comboBox1.SelectedIndex)
            {
                case 1: name = "Login"; break;
                case 2: name = "World"; break;
                case 3: name = "Chat"; break;
            }

            for (int i = 0; i < dgvPackets.RowCount; i ++)
            {
                if (name == "")
                    dgvPackets.Rows[i].Visible = true;
                else
                {
                    if (dgvPackets.Rows[i].HeaderCell.Value.ToString() == name)
                        dgvPackets.Rows[i].Visible = true;
                    else
                        dgvPackets.Rows[i].Visible = false;
                }

            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}