﻿/*
    Copyright © 2010 The Divinity Project; 2013-2016 Dignity Team.
	All rights reserved.
	http://board.thedivinityproject.com
	http://www.ragezone.com


	This file is part of Ostara.

	Ostara is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Ostara is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Ostara.  If not, see <http://www.gnu.org/licenses/>.
*/

#region Includes

using System;

#endregion

namespace CabalPacketPal.Cryption
{
	public class Server
	{
		public byte[] Keychain;
		uint[] Keys2 = { 0xFFFFFFFF, 0xFFFFFF00, 0xFFFF0000, 0xFF000000 };

		public uint Step;
		public uint Mul;

		public Server()
		{
			Keychain = new byte[0x20000];
			GenerateKeychain(0x8F54C37B, 0, 0x4000);

			Step = 0;
			Mul = 1;
		}

		public void Decrypt(ref byte[] packet)
		{
			uint size = (uint)packet.Length;
			Array.Resize(ref packet, packet.Length + 4);
			uint i = 4;
			uint Key = BitConverter.ToUInt32(Keychain, (BitConverter.ToInt32(packet, 0) & 0x3FFF) * 4);
			BitConverter.GetBytes(BitConverter.ToUInt32(packet, 0) ^ 0x7AB38CF1).CopyTo(packet, 0);
			uint t = (size - 4) >> 2; //Shift right 2 = divide by 4
			uint t1;

			while (t > 0)
			{
				t1 = BitConverter.ToUInt32(packet, (int)i);
				Key = Key ^ t1;
				BitConverter.GetBytes(Key).CopyTo(packet, i);
				t1 = t1 & 0x3FFF;
				Key = BitConverter.ToUInt32(Keychain, (int)(t1 * 4));
				i += 4;
				t--;
			}

			t1 = Keys2[((size - 4) & 3)];
			t1 = ~t1;
			t1 = t1 & Key;
			BitConverter.GetBytes(BitConverter.ToUInt32(packet, (int)i) ^ t1).CopyTo(packet, i);
			Array.Resize(ref packet, (int)size);
		}

		public void GenerateKeychain(uint key, int position, int size)
		{
			uint ret2;
			uint ret3;
			uint ret4;

			for (int i = position; i < position + size; i++)
			{
				ret4 = key * 0x2F6B6F5;
				ret4 += 0x14698B7;
				ret2 = ret4;
				ret4 = ret4 >> 0x10;
				ret4 = ret4 * 0x27F41C3;
				ret4 += 0x0B327BD;
				ret4 = ret4 >> 0x10;

				ret3 = ret2 * 0x2F6B6F5;
				ret3 += 0x14698B7;
				key = ret3;
				ret3 = ret3 >> 0x10;
				ret3 = ret3 * 0x27F41C3;
				ret3 += 0x0B327BD;
				ret3 = ret3 >> 0x10;
				ret3 = ret3 << 0x10;

				ret4 = ret4 | ret3;
				BitConverter.GetBytes(ret4).CopyTo(Keychain, i * 4);
			}
		}

		public void ChangeKey(uint key, uint step)
		{
			Mul = 2;
			Step = (uint)(step - 1);
			if ((int)Step < 0)
				Step = (uint)((int)Step + 0x4000);

			GenerateKeychain(key, 0x4000, 0x4000);
		}

		public int GetPacketSize(ref byte[] packet)
		{
			var header = BitConverter.ToUInt32(packet, 0);

			header ^= 0x7AB38CF1;
			header >>= 16;

			return (int)header;
		}

		public bool IsValid(ref byte[] packet)
		{
			var header = packet[0];

			header ^= 0xF1;

			return (header == 0xE2);
		}
	}
}