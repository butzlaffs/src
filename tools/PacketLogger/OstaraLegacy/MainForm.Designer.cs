﻿namespace CabalPacketPal
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lblNI = new System.Windows.Forms.Label();
            this.btnGo = new System.Windows.Forms.Button();
            this.comboDevices = new System.Windows.Forms.ComboBox();
            this.dgvPackets = new System.Windows.Forms.DataGridView();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SrcIp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SrcPort = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DestIp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DestPort = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Opcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitPacket = new System.Windows.Forms.SplitContainer();
            this.splitHex = new System.Windows.Forms.SplitContainer();
            this.txtLines = new System.Windows.Forms.TextBox();
            this.rtxtHex = new System.Windows.Forms.RichTextBox();
            this.txtText = new System.Windows.Forms.TextBox();
            this.cmsRow = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiAddIgnore = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiClearIgnore = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEditSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEditClear = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiEditPause = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).BeginInit();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.Panel2.SuspendLayout();
            this.splitMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPackets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPacket)).BeginInit();
            this.splitPacket.Panel1.SuspendLayout();
            this.splitPacket.Panel2.SuspendLayout();
            this.splitPacket.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitHex)).BeginInit();
            this.splitHex.Panel1.SuspendLayout();
            this.splitHex.Panel2.SuspendLayout();
            this.splitHex.SuspendLayout();
            this.cmsRow.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitMain
            // 
            this.splitMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.Location = new System.Drawing.Point(0, 0);
            this.splitMain.Name = "splitMain";
            this.splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.Controls.Add(this.splitContainer1);
            // 
            // splitMain.Panel2
            // 
            this.splitMain.Panel2.Controls.Add(this.splitPacket);
            this.splitMain.Size = new System.Drawing.Size(784, 562);
            this.splitMain.SplitterDistance = 400;
            this.splitMain.SplitterWidth = 2;
            this.splitMain.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.comboBox1);
            this.splitContainer1.Panel1.Controls.Add(this.lblNI);
            this.splitContainer1.Panel1.Controls.Add(this.btnGo);
            this.splitContainer1.Panel1.Controls.Add(this.comboDevices);
            this.splitContainer1.Panel1MinSize = 20;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvPackets);
            this.splitContainer1.Size = new System.Drawing.Size(782, 398);
            this.splitContainer1.SplitterDistance = 25;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 1;
            // 
            // comboBox1
            // 
            this.comboBox1.DisplayMember = "1";
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 12;
            this.comboBox1.Items.AddRange(new object[] {
            "All",
            "Login",
            "World",
            "Chat"});
            this.comboBox1.Location = new System.Drawing.Point(312, 5);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(75, 20);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // lblNI
            // 
            this.lblNI.AutoSize = true;
            this.lblNI.Location = new System.Drawing.Point(4, 8);
            this.lblNI.Name = "lblNI";
            this.lblNI.Size = new System.Drawing.Size(95, 13);
            this.lblNI.TabIndex = 2;
            this.lblNI.Text = "Network Interface:";
            // 
            // btnGo
            // 
            this.btnGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGo.BackColor = System.Drawing.Color.ForestGreen;
            this.btnGo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGo.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnGo.Location = new System.Drawing.Point(681, 3);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(90, 22);
            this.btnGo.TabIndex = 1;
            this.btnGo.Text = "Start Logging";
            this.btnGo.UseVisualStyleBackColor = false;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // comboDevices
            // 
            this.comboDevices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDevices.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboDevices.FormattingEnabled = true;
            this.comboDevices.ItemHeight = 12;
            this.comboDevices.Location = new System.Drawing.Point(104, 5);
            this.comboDevices.Name = "comboDevices";
            this.comboDevices.Size = new System.Drawing.Size(202, 20);
            this.comboDevices.TabIndex = 0;
            // 
            // dgvPackets
            // 
            this.dgvPackets.AllowUserToAddRows = false;
            this.dgvPackets.AllowUserToDeleteRows = false;
            this.dgvPackets.AllowUserToResizeColumns = false;
            this.dgvPackets.AllowUserToResizeRows = false;
            this.dgvPackets.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvPackets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPackets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Time,
            this.Size,
            this.SrcIp,
            this.SrcPort,
            this.DestIp,
            this.DestPort,
            this.Opcode});
            this.dgvPackets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPackets.Location = new System.Drawing.Point(0, 0);
            this.dgvPackets.MultiSelect = false;
            this.dgvPackets.Name = "dgvPackets";
            this.dgvPackets.ReadOnly = true;
            this.dgvPackets.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dgvPackets.Size = new System.Drawing.Size(782, 372);
            this.dgvPackets.TabIndex = 0;
            this.dgvPackets.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPackets_CellMouseUp);
            this.dgvPackets.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPackets_RowEnter);
            // 
            // Time
            // 
            this.Time.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Time.HeaderText = "Time";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Time.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Time.Width = 36;
            // 
            // Size
            // 
            this.Size.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Size.HeaderText = "Size";
            this.Size.Name = "Size";
            this.Size.ReadOnly = true;
            this.Size.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Size.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Size.Width = 33;
            // 
            // SrcIp
            // 
            this.SrcIp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.SrcIp.HeaderText = "Source IP";
            this.SrcIp.Name = "SrcIp";
            this.SrcIp.ReadOnly = true;
            this.SrcIp.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SrcIp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SrcIp.Width = 60;
            // 
            // SrcPort
            // 
            this.SrcPort.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.SrcPort.HeaderText = "Source Port";
            this.SrcPort.Name = "SrcPort";
            this.SrcPort.ReadOnly = true;
            this.SrcPort.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SrcPort.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SrcPort.Width = 69;
            // 
            // DestIp
            // 
            this.DestIp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.DestIp.HeaderText = "Destination IP";
            this.DestIp.Name = "DestIp";
            this.DestIp.ReadOnly = true;
            this.DestIp.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DestIp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DestIp.Width = 79;
            // 
            // DestPort
            // 
            this.DestPort.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.DestPort.HeaderText = "Destination Port";
            this.DestPort.Name = "DestPort";
            this.DestPort.ReadOnly = true;
            this.DestPort.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DestPort.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DestPort.Width = 88;
            // 
            // Opcode
            // 
            this.Opcode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Opcode.HeaderText = "Opcode";
            this.Opcode.Name = "Opcode";
            this.Opcode.ReadOnly = true;
            this.Opcode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Opcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // splitPacket
            // 
            this.splitPacket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitPacket.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitPacket.IsSplitterFixed = true;
            this.splitPacket.Location = new System.Drawing.Point(0, 0);
            this.splitPacket.Name = "splitPacket";
            // 
            // splitPacket.Panel1
            // 
            this.splitPacket.Panel1.Controls.Add(this.splitHex);
            // 
            // splitPacket.Panel2
            // 
            this.splitPacket.Panel2.Controls.Add(this.txtText);
            this.splitPacket.Size = new System.Drawing.Size(782, 158);
            this.splitPacket.SplitterDistance = 582;
            this.splitPacket.TabIndex = 0;
            // 
            // splitHex
            // 
            this.splitHex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitHex.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitHex.Location = new System.Drawing.Point(0, 0);
            this.splitHex.Name = "splitHex";
            // 
            // splitHex.Panel1
            // 
            this.splitHex.Panel1.Controls.Add(this.txtLines);
            // 
            // splitHex.Panel2
            // 
            this.splitHex.Panel2.Controls.Add(this.rtxtHex);
            this.splitHex.Size = new System.Drawing.Size(582, 158);
            this.splitHex.SplitterDistance = 64;
            this.splitHex.TabIndex = 0;
            // 
            // txtLines
            // 
            this.txtLines.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLines.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLines.Enabled = false;
            this.txtLines.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLines.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtLines.Location = new System.Drawing.Point(0, 0);
            this.txtLines.Multiline = true;
            this.txtLines.Name = "txtLines";
            this.txtLines.Size = new System.Drawing.Size(64, 158);
            this.txtLines.TabIndex = 0;
            // 
            // rtxtHex
            // 
            this.rtxtHex.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtHex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtHex.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtHex.Location = new System.Drawing.Point(0, 0);
            this.rtxtHex.Name = "rtxtHex";
            this.rtxtHex.ReadOnly = true;
            this.rtxtHex.Size = new System.Drawing.Size(514, 158);
            this.rtxtHex.TabIndex = 0;
            this.rtxtHex.Text = "";
            // 
            // txtText
            // 
            this.txtText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtText.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtText.Location = new System.Drawing.Point(0, 0);
            this.txtText.Multiline = true;
            this.txtText.Name = "txtText";
            this.txtText.ReadOnly = true;
            this.txtText.Size = new System.Drawing.Size(196, 158);
            this.txtText.TabIndex = 0;
            // 
            // cmsRow
            // 
            this.cmsRow.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAddIgnore,
            this.toolStripSeparator1,
            this.tsmiClearIgnore,
            this.toolStripSeparator2,
            this.tsmiEdit});
            this.cmsRow.Name = "cmsRow";
            this.cmsRow.Size = new System.Drawing.Size(169, 82);
            // 
            // tsmiAddIgnore
            // 
            this.tsmiAddIgnore.Name = "tsmiAddIgnore";
            this.tsmiAddIgnore.Size = new System.Drawing.Size(168, 22);
            this.tsmiAddIgnore.Text = "Add to Ignore List";
            this.tsmiAddIgnore.Click += new System.EventHandler(this.tsmiAddIgnore_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(165, 6);
            // 
            // tsmiClearIgnore
            // 
            this.tsmiClearIgnore.Name = "tsmiClearIgnore";
            this.tsmiClearIgnore.Size = new System.Drawing.Size(168, 22);
            this.tsmiClearIgnore.Text = "Clear Ignore List";
            this.tsmiClearIgnore.Click += new System.EventHandler(this.tsmiClearIgnore_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(165, 6);
            // 
            // tsmiEdit
            // 
            this.tsmiEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiEditSave,
            this.tsmiEditClear,
            this.toolStripSeparator3,
            this.tsmiEditPause});
            this.tsmiEdit.Name = "tsmiEdit";
            this.tsmiEdit.Size = new System.Drawing.Size(168, 22);
            this.tsmiEdit.Text = "Edit";
            // 
            // tsmiEditSave
            // 
            this.tsmiEditSave.Name = "tsmiEditSave";
            this.tsmiEditSave.Size = new System.Drawing.Size(152, 22);
            this.tsmiEditSave.Text = "&Save List";
            this.tsmiEditSave.Click += new System.EventHandler(this.tsmiEditSave_Click);
            // 
            // tsmiEditClear
            // 
            this.tsmiEditClear.Name = "tsmiEditClear";
            this.tsmiEditClear.Size = new System.Drawing.Size(152, 22);
            this.tsmiEditClear.Text = "&Clear List";
            this.tsmiEditClear.Click += new System.EventHandler(this.tsmiEditClear_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(149, 6);
            // 
            // tsmiEditPause
            // 
            this.tsmiEditPause.Name = "tsmiEditPause";
            this.tsmiEditPause.Size = new System.Drawing.Size(152, 22);
            this.tsmiEditPause.Text = "Pause Logging";
            this.tsmiEditPause.Click += new System.EventHandler(this.tsmiEditPause_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.splitMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ostara :: CABAL Online Packet Logger";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.splitMain.Panel1.ResumeLayout(false);
            this.splitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).EndInit();
            this.splitMain.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPackets)).EndInit();
            this.splitPacket.Panel1.ResumeLayout(false);
            this.splitPacket.Panel2.ResumeLayout(false);
            this.splitPacket.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPacket)).EndInit();
            this.splitPacket.ResumeLayout(false);
            this.splitHex.Panel1.ResumeLayout(false);
            this.splitHex.Panel1.PerformLayout();
            this.splitHex.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitHex)).EndInit();
            this.splitHex.ResumeLayout(false);
            this.cmsRow.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitMain;
		private System.Windows.Forms.SplitContainer splitPacket;
		private System.Windows.Forms.DataGridView dgvPackets;
		private System.Windows.Forms.TextBox txtText;
		private System.Windows.Forms.RichTextBox rtxtHex;
		private System.Windows.Forms.DataGridViewTextBoxColumn PacketSize;
		private System.Windows.Forms.SplitContainer splitHex;
		private System.Windows.Forms.TextBox txtLines;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.Button btnGo;
		private System.Windows.Forms.ComboBox comboDevices;
		private System.Windows.Forms.Label lblNI;
		private System.Windows.Forms.DataGridViewTextBoxColumn Time;
		private System.Windows.Forms.DataGridViewTextBoxColumn Size;
		private System.Windows.Forms.DataGridViewTextBoxColumn SrcIp;
		private System.Windows.Forms.DataGridViewTextBoxColumn SrcPort;
		private System.Windows.Forms.DataGridViewTextBoxColumn DestIp;
		private System.Windows.Forms.DataGridViewTextBoxColumn DestPort;
		private System.Windows.Forms.DataGridViewTextBoxColumn Opcode;
		private System.Windows.Forms.ContextMenuStrip cmsRow;
		private System.Windows.Forms.ToolStripMenuItem tsmiAddIgnore;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem tsmiClearIgnore;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem tsmiEdit;
		private System.Windows.Forms.ToolStripMenuItem tsmiEditSave;
		private System.Windows.Forms.ToolStripMenuItem tsmiEditClear;
		private System.Windows.Forms.ToolStripMenuItem tsmiEditPause;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}

