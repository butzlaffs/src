# Minerva #

This is Minerva - CABAL Online Server Emulator, written in C# language. Initially started by The Divinity Project, now being managed by Dignity Team.

### Information ###

* It's open-source and always will be. 
* It's not a beta. It's something like pre-alpha, buggy, small functionality(yet).
* Currently Login, Channel(World) and Master(Database/Sync) servers are working.
* Using MySQL as database. Dropped support for MSSQL.
* Using **EP15** as a base.
* As a client, using official [CABAL EU](http://cabaleu.estgames.com/en/download).
* Yes, scripting will be available for adding npc's and other functionality. But for the moment, it's not a essential part and won't be targeted.
* Current Status: [Status](https://bitbucket.org/dignityteam/minerva/wiki/Status)

### Client, and other stuff ###

* [Download CABAL EU EP15 Primary](http://cabaleu.estgames.com/en/FullClient/WEST).
* [Download CABAL EU EP15 Secondary](http://cabaleu.estgames.com/en/FullClient/GLOBAL).
---
* [Download CABAL EU EP8](http://www.mediafire.com/download/d6s03a5h3wlx3x3/CABAL_Online_Europe_Full_Installer.7z).
* [Unpacked, GG removed main v1421 EP8](https://bitbucket.org/dignityteam/minerva/downloads/fixedmain.zip).

### Important ###

* **Always** use latest commit.
* Keep your databases up-date to the latest structures in bin/sql directory.


### Compatible with ###

* Official client support is CABAL EU EP15;
* .NET Framework 4.6;
* Mono 4.2.x;
* MariaDB (you can grab xammp with all required tools [here](https://www.apachefriends.org/download.html));
* MySQL 5.6 or newer (partially)


### Tools ###

* **scp2xml** - it's used to convert *.scp config files to Minerva's XML format. Manager: GoldenHeaven.
* **PacketLogger** - or just Ostara Legacy(old one). Outdated, not supported. **Use only for educational or research purposes!** 
* **Newer Ostara** - please refer to [Downloads page](https://bitbucket.org/dignityteam/minerva/downloads), or grab source [right here](https://github.com/Epidal/Ostara).


### Versions ###


* [Minerva v1.0](https://bitbucket.org/dignityteam/minerva/commits/tag/v1.0) - EP8, unsupported.
* [Minerva v2.0](https://bitbucket.org/dignityteam/minerva/commits/tag/v2.0) - EP8, unsupported, last pre-compiled [revision](https://bitbucket.org/dignityteam/minerva/downloads/Minerva%20v2.0-b3f3eaf.zip).
* [Minerva v2.1](https://bitbucket.org/dignityteam/minerva/commits/all) - EP14, unsupported, no pre-compiled bin's yet.
* [Minerva Latest](https://bitbucket.org/dignityteam/minerva/commits/all) - EP15, in-development, last [pre-compiled bin's](http://dev.emanuelscirlet.com/repository/downloadAll/Minerva_Build/.lastSuccessful/?guest=1).
* [Working Status of Minerva Latest bin's](http://dev.emanuelscirlet.com/project.html?guest=1&projectId=Minerva&tab=projectOverview) - Check if Minerva Latest bins are working


### ###

Copyright © 2010 The Divinity Project; 2013-2016 Dignity Team.